<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "salas".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $butacas
 * @property int|null $pelicula
 *
 * @property Peliculas $pelicula0
 */
class Salas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'salas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['butacas', 'pelicula'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['pelicula'], 'exist', 'skipOnError' => true, 'targetClass' => Peliculas::className(), 'targetAttribute' => ['pelicula' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'butacas' => 'Butacas',
            'pelicula' => 'Pelicula',
        ];
    }

    /**
     * Gets query for [[Pelicula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPelicula0()
    {
        return $this->hasOne(Peliculas::className(), ['id' => 'pelicula']);
    }
}
