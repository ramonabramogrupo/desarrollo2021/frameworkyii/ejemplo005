<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Salas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="salas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'butacas')->textInput() ?>

        
    <?php 
        // array de modelos con todas las peliculas
        $peliculas= app\models\Peliculas::find()->all();

        // crear un array asociativo con las pelis
        $listData= \yii\helpers\ArrayHelper::map($peliculas,'id','titulo');

        echo $form->field($model, 'pelicula')->dropDownList(
            $listData,
            ['prompt'=>'Selecciona una pelicula']
        );
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
